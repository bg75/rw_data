def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return ff


def read_matlab_file(infile):
    """read MATLAB input in v5 and v7.3 format

    :param infile: input file (str)
    :return: mat_dict
    """

    # import packages
    from scipy.io import loadmat
    import h5py

    try:
        mat_dict = loadmat(infile)
        return mat_dict
    except NotImplementedError:
        f = h5py.File(infile)
        mat_dict = dict(f)
        return mat_dict
    except:
        print('Not a proper .mat file.\n')
